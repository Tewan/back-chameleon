#API Chameleon


<img src="logo-chameleon.jpg" width=150>
  

##Technologies utilisées

  
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/590px-Node.js_logo.svg.png" width=100>
<img src="https://miro.medium.com/max/816/1*mn6bOs7s6Qbao15PMNRyOA.png" width=100>
<img src="https://upload.wikimedia.org/wikipedia/fr/thumb/4/45/MongoDB-Logo.svg/527px-MongoDB-Logo.svg.png" width=200>
<img src="https://expressjs.com/images/express-facebook-share.png" width=200>
  
Pour cette API, nous avons choisi d'utiliser un serveur NodeJS (+ Express) en développant en TypeScript afin de produire un code de qualité.
En base de données, nous utilisons MongoDB.

##Endpoints de l'API
###Utilisateurs (entreprise de revente de montre)
- Création d'un utilisateur
POST https://frozen-waters-50176.herokuapp.com/user
BODY email, password
- Récupération de tous les utilisateurs
GET https://frozen-waters-50176.herokuapp.com/user
- Récupération de l'utilisateur par son id
GET https://frozen-waters-50176.herokuapp.com/user/{id}
- Suppression d'un utilisateur par son id
DELETE https://frozen-waters-50176.herokuapp.com/user/{id}
- Connexion de l'utilisateur
POST https://frozen-waters-50176.herokuapp.com/user/login
BODY email, password

###Commandes (de montres)
TODO

##Swagger
Swagger disponible

##Déploiement continu sur Heroku
Après chaque commit, l'API est directement re-déployé sur Heroku à l'adresse : https://frozen-waters-50176.herokuapp.com/
