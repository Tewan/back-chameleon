import * as express from 'express'
import * as expressFormidable from 'express-formidable'
import * as cors from 'cors'
import * as helmet from 'helmet'
import * as swaggerUi from 'swagger-ui-express'
import * as mongoose from 'mongoose'
import * as expressPinoLogger from 'express-pino-logger'


// Nécessaire d'importer les controllers afin qu'ils soient crawlés
import './controllers/user'
import './controllers/order'
import './controllers/client'
import './controllers/devis'
import './controllers/bracelet'
import './controllers/cadran'
import './controllers/facture'

import { RegisterRoutes } from './routes';
import { logger } from './utils/Logger';


const MONGO_URI = "mongodb+srv://chameleon:chameleon@cluster0-vft4i.mongodb.net/test?retryWrites=true&w=majority";
/**
 * App
 * Classe de l'application
 */
class App {
    /** app -> Express.Application */
    public app;

    constructor() {
        this.app = express();
        this.config();
        this.swagger();
        this.routes();
        this.connectToMongoDB();
    }

    /** Méthode de configuration de l'application */
    private config(): void {
        this.app.use(expressFormidable());
        this.app.use(cors());
        this.app.use(helmet());
        this.app.use(expressPinoLogger({ logger: logger }));
    }

    private routes(): void {
        RegisterRoutes(this.app);
    }

    private swagger(): void {
        try {
            const swaggerDocument = require('./swagger.json');
            this.app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
        } catch (err) {
            console.log('Impossible de charger swagger.json', err);
        }
    }

    private connectToMongoDB(): void {
        mongoose.set('useCreateIndex', true);
        mongoose.connect(MONGO_URI, { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true});
        mongoose.connection.on('open', () => {
            console.info('Connecté à MongoDB !');
        });
        mongoose.connection.on('error', (err: any) => {
            console.error(err);
        });
    }
}

export default new App().app;