import * as mongoose from 'mongoose';

const CadranSchema = new mongoose.Schema({
    couleur: {type: String, required: true},
    forme: {type: String, required: true},
    prix: {type: Number}
});

const CadranModel = mongoose.model('Cadran', CadranSchema);

export { CadranModel }