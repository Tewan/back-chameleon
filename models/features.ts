import * as mongoose from 'mongoose';

const FeaturesSchema = new mongoose.Schema({
    nom: {type: String, required: true},
    prix: {type: Number}
});

const FeaturesModel = mongoose.model('Features', FeaturesSchema);

export { FeaturesModel }