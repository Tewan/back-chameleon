import * as mongoose from 'mongoose'

const DevisSchema: mongoose.Schema = new mongoose.Schema({    
    pdf: {type: String},
    snippet: {type: String},
    status: {type: String, required: true},
    order_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Order', required: true},
    company_id: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    client_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Client', required: true}
},
{
    timestamps: true
});

const DevisModel = mongoose.model('Devis', DevisSchema);

export { DevisModel };

