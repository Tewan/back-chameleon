import * as mongoose from 'mongoose';
import isEmail from 'validator/lib/isEmail'

const ClientSchema: mongoose.Schema = new mongoose.Schema({
    lastname: { type: String, required: true },
    firstname: { type: String, required: true },
    email: { type: String, required: true, unique: true, validate: isEmail},
    adress: { type: String, required: true},
    phoneNumber: { type: String, required: true},
    companies: {type: [mongoose.Schema.Types.ObjectId], ref: 'User'},
    orders: {type: [mongoose.Schema.Types.ObjectId], ref: 'Order'}
});

const ClientModel = mongoose.model('Client', ClientSchema);


export { ClientSchema, ClientModel }