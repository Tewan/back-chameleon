import * as mongoose from 'mongoose'

const FactureSchema: mongoose.Schema = new mongoose.Schema({    
    pdf: {type: String},
    snippet: {type: String},
    order_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Order', required: true},
    devis_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Devis', required: true},
    company_id: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    client_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Client', required: true}
},
{
    timestamps: true
});

const FactureModel = mongoose.model('Facture', FactureSchema);

export { FactureModel };

