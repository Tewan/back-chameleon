import * as mongoose from 'mongoose'

const OrderSchema: mongoose.Schema = new mongoose.Schema({
    montre: new mongoose.Schema({
        bracelet: {type: String, required: true},
        cadran: new mongoose.Schema({
            couleur: {type: String, required: true},
            forme: {type: String, required: true}
        }),
        procede: {type: String, required: true},
        features: [{type: String}]
    }),
    prix: {type: Number, required: true},
    company_id: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    client_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Client', required: true}
},
{
    timestamps: true
});

const OrderModel = mongoose.model('Order', OrderSchema);

export { OrderModel };

