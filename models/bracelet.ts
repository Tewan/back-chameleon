import * as mongoose from 'mongoose';

const BraceletSchema = new mongoose.Schema({
    lib_long: {type: String, required: true},
    lib_court: {type: String, required: true},
    prix: {type: Number}
});

const BraceletModel = mongoose.model('Bracelet', BraceletSchema);

export { BraceletSchema, BraceletModel }