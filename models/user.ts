import * as mongoose from 'mongoose'
import isEmail from 'validator/lib/isEmail'

const UserSchema: mongoose.Schema = new mongoose.Schema({
    avatar: { type: String },
    companyName: { type: String, required: true },
    email: { type: String, required: true, unique: true, validate: isEmail},
    password: { type: String, required: true},
    adress: { type: String, required: true},
    phoneNumber: { type: String, required: true}
});

const UserModel = mongoose.model('User', UserSchema);

export { UserModel, UserSchema };

