import { Controller, Route, Tags, Post, BodyProp, SuccessResponse, Response, Get, Query, Delete, Request } from 'tsoa'
import { Types } from 'mongoose'
import * as puppeteer from 'puppeteer'
import * as moment from 'moment'
import { OrderModel } from '../models/order'
import { UserModel } from '../models/user'
import { ClientModel } from '../models/client'
import { IResponse } from '../interfaces/IResponse'
import httpStatus from '../utils/HttpStatus'

import { BraceletModel } from '../models/bracelet'
import { FeaturesModel } from '../models/features'
import { DevisModel } from '../models/devis'
import { CadranModel } from '../models/cadran'

@Route('/order')
@Tags('Order')
export class OrderController extends Controller {

    @Post('/')
    @SuccessResponse(httpStatus[201], 'Commande créée')
    @Response<IResponse>('500', 'Erreur MongoDB lors de la création')
    public async createOrder(@Request() req): Promise<IResponse> {
        try {
            // Test si l'entreprise existe en base avant de créer la commande !!
            let company: any = await UserModel.findOne({_id: req.fields.idCompany});
            if (company) {
                let montre = { bracelet: req.fields.bracelet, cadran: req.fields.cadran, features: req.fields.features,
                    procede: req.fields.procede};
                // TODO Calcul du prix de la montre
                let prix = await this.calculPrixMontre(req.fields.bracelet, req.fields.cadran.forme, req.fields.cadran.couleur,
                    req.fields.features);
                let newOrder = new OrderModel({montre: montre, prix: prix, company_id: new Types.ObjectId(req.fields.idCompany),
                    client_id: new Types.ObjectId(req.fields.idClient)});
                let orderSaved = await newOrder.save();
                // Récupération des infos du client
                try {
                    let clientMongoose = await ClientModel.findOne({_id: req.fields.idClient})
                    let responseClient: IResponse = {message: '', statusCode: 200, data: clientMongoose}
                    let client = responseClient.data;
                    try {
                        let newDevis = new DevisModel({status: "NON SIGNÉ", client_id: req.fields.idClient, company_id: req.fields.idCompany,
                            order_id: orderSaved._id});
                        let devisSaved = await newDevis.save();
                        let date = moment(devisSaved['createdAt']).format('DD/MM/YYYY');
                        // Générer devis
                        try {
                            let generatedDoc = await this.generatePdfAndScreen(client, devisSaved._id, date
                                , company['companyName'], company['adress'], req.fields.bracelet, req.fields.cadran
                                , req.fields.procede, req.fields.features, prix);
                            // Enregistrement du devis
                            await DevisModel.findOneAndUpdate({_id: devisSaved._id}, {$set: generatedDoc});
                        }
                        catch(errMajDevis) {
                            let responseErr: IResponse = {message: httpStatus[500], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
                                data: "errMajDevis"};
                            this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
                            return responseErr;
                        }
                    }
                    catch(errDevis) {
                        let responseErr: IResponse = {message: httpStatus[500], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
                            data: "errDevis"};
                        this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
                        return responseErr;
                    }
                }
                catch(errClient) {
                    let responseErr: IResponse = {message: httpStatus[500], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
                        data: "errClient"};
                    this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
                    return responseErr;
                }
                let responseOk: IResponse = {message: httpStatus[201], statusCode: httpStatus.CREATED,
                    data: newOrder};
                this.setStatus(httpStatus.CREATED);
                return responseOk;
            } else {
                let responseCompanyNotFound: IResponse = {message: httpStatus[404], statusCode: httpStatus.NOT_FOUND,
                    data: 'Company not found'};
                return responseCompanyNotFound;
            }
        }
        catch(err) {
            let responseErr: IResponse = {message: httpStatus[500], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
            data: err};
            this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
            return responseErr;
        }
    }

    @Get('/lol')
    public async getAllOrders(): Promise<IResponse> {
        try {
            let orders = await OrderModel.find();
            let responseOk: IResponse = { message: httpStatus[200], statusCode: httpStatus.OK, data: orders };
            this.setStatus(httpStatus.OK);
            return responseOk;
        } catch(err) {
            let responseErr: IResponse = { message: httpStatus[404], statusCode: httpStatus.NOT_FOUND, data: 'R' };
            this.setStatus(httpStatus.NOT_FOUND);
            return responseErr;
        }
    }

    @Delete('/lol2')
    public async deleteAllOrders(): Promise<IResponse> {
        try {
            let data = await OrderModel.deleteMany({});
            let responseOk: IResponse = { message: httpStatus[200], statusCode: httpStatus.OK, data: data };
            this.setStatus(httpStatus.OK);
            return responseOk;
        } catch(err) {
            let responseErr: IResponse = { message: httpStatus[404], statusCode: httpStatus.NOT_FOUND, data: '' };
            this.setStatus(httpStatus.NOT_FOUND);
            return responseErr;
        }
    }

    @Get('/{id}')
    @SuccessResponse('200', 'Commande récupérée')
    @Response<IResponse>('404', 'Aucune commande trouvée')
    public async getOrderById(id: string): Promise<IResponse> {
        try {
            let order = await OrderModel.findById(id);
            let bracelet = await BraceletModel.findOne({lib_court: order['montre']['bracelet']});
            let orderRes = order;
            orderRes['montre']['bracelet'] = bracelet['lib_long'];
            let responseOk: IResponse = { message: httpStatus[200], statusCode: httpStatus.OK, data: orderRes };
            this.setStatus(httpStatus.OK);
            return responseOk;
        } catch(err) {
            let responseErr: IResponse = { message: httpStatus[404], statusCode: httpStatus.NOT_FOUND, data: err };
            this.setStatus(httpStatus.NOT_FOUND);
            return responseErr;
        }
    }

    @Get('/')
    @SuccessResponse('200', 'Commande(s) récupérée(s)')
    @Response<IResponse>('404', 'Aucune entreprise trouvée')
    @Response<IResponse>('404', 'Aucune commande trouvée')
    public async getOrdersByCompanyId(@Query() idCompany: string) {
        try {
            let company: any = await UserModel.findOne({_id: idCompany});
            if (company) {
                try {
                    let orders = await OrderModel.find({}).populate('user', null, {company_id: idCompany});
                    let responseOk: IResponse = { message: httpStatus[200], statusCode: httpStatus.OK, data: orders };
                    this.setStatus(httpStatus.OK);
                    return responseOk;
                }
                catch (err) {
                    let responseErr: IResponse = { message: httpStatus[404], statusCode: httpStatus.NOT_FOUND, data:'Aucune commande trouvée' };
                    this.setStatus(httpStatus.NOT_FOUND);
                    return responseErr;
                }
            }
        } catch(err) {
            let responseErr: IResponse = { message: httpStatus[404], statusCode: httpStatus.NOT_FOUND, data: 'Aucune entreprise trouvé'};
            this.setStatus(httpStatus.NOT_FOUND)
            return responseErr;
        }
    }

    private async calculPrixMontre(braceletLibCourt: string, cadranForme: string, cadranCouleur: string,
            features: [string]): Promise<number> { 
        let braceletDb = await BraceletModel.findOne({lib_court: braceletLibCourt});
        let prixBracelet: number = braceletDb["prix"];
        let cadranDb = await await CadranModel.findOne({couleur: cadranCouleur.toLowerCase()
            , forme: cadranForme.toLowerCase()});
        let prixCadran: number = cadranDb['prix'];
        const prixProcede: number = 50.00;
        let prixFeatures: number = features.length * 10.00;
        let prixTotal: number = prixBracelet + prixCadran + prixProcede + prixFeatures;
        return prixTotal;
    }

    private featuresToRow(features: [string]): string {
        let templateHtml = []
        features.forEach(feature => {
            let tr: string = "<tr class='l4'/>";
            let td1: string = "<td id='option' style='width: 50%'>"+ feature +"</td>";
            let td2: string = "<td id='o_nb' style='text-align: right;''>1</td>";
            let td3: string = "<td id='o_prix' style='text-align: right;''>10.00</td>";
            let trFin: string = "</tr>";
            templateHtml.push(tr, td1, td2, td3, trFin);
        });
        let html: string = templateHtml.join("");
        return html;
    }

    private async generatePdfAndScreen(client, devis_id: string, date: string, companyName: string
            , companyAdress: string, bracelet: string, cadran, procede: string, features: [string], prix: number) {
        let braceletDb = await BraceletModel.findOne({lib_court: bracelet});
        let cadranDb = await CadranModel.findOne({forme: cadran.forme, couleur: cadran.couleur.toLowerCase()});
        let templateHtml = [];
        templateHtml.push(
            "<!DOCTYPE html>",
            "<html>",
            "<head>",
            "<meta charset='UTF-8'>",
            "<meta name='viewport' content='width=device-width, initial-scale=1.0'>",
            "<title>Devis</title>",
            "</head>",
            "<body style='font-family: Arial, Helvetica, sans-serif; width: 210mm; height: 297mm; margin: auto;'>",
            "<div id='page' style='position: relative; width: 210mm; height: 297mm; box-sizing: border-box; padding-top: 15mm; padding-left: 20mm; padding-right: 20mm; padding-bottom: 15mm; background-color: white;'>",
            "<h1 style='float: right; margin: 0; font-size: 30px;'>DEVIS</h1>",
            "<h2 style='margin: 0; font-size: 30px;'>"+ client.firstname + " " + client.lastname + "</h2>",
            "<div style='clear: both;'></div>",
            "<table style='margin-top: 20px;'>",
            "<tr>",
            "<td id='adress_1'>" + client.adress + "</td>",
            "</tr>",
            "</table>",
            "<table style='float: right; text-align: right; margin-top: 150px;'>",
            "<tr>",
            "<th>Devis n°</th>",
            "<td id='devis_nb'>"+ devis_id + "</td>",
            "</tr>",
            "<th>Date du devis</th>",
            "<td id='devis_date'>" + date + "</td>",
            "</table>",
            "<p style='margin-top: 150px;'><strong>Facturé à</strong><br>" + companyName + "<br>" + companyAdress + "</p>",
            "<div style='clear: both;'></div>",
            "<table class='main' style ='margin-top: 50px; border-collapse: collapse; width: 100%;'>",
            "<style>",
            "html, body {height: 100%;}",
            ".l0, .l1, .l2, .l3, .l4, .l0 th, .l1 td, .l2 td, .l3 td, .l4 td{ border: thin solid black;}",
            ".main th, .main td { padding-top: 10px; padding-bottom: 10px;}",
            ".main td { padding-left: 15px; padding-right: 15px; padding-top: 10px; padding-bottom: 10px; }",
            "</style>",
            "<tr class='l0' style='background-color:#ededed;'>",
            "<th>DÉSIGNATION</th>",
            "<th>QUANTITÉ</th>",
            "<th>MONTANT</th>",
            "</tr>",
            "<tr class='l1'>",
            "<td id='bracelet'>" + braceletDb["lib_long"] + "</td>",
            "<td id='b_nb' style='text-align: right;''>1</td>",
            "<td id='b_prix' style='text-align: right;'>"+ braceletDb["prix"] +"</td>",
            "</tr>",
            "<tr class='l2'>",
            "<td id='cadran'>Cadran " + cadranDb["forme"] + "couleur " + cadranDb["couleur"].toLowerCase() +"</td>",
            "<td id='c_nb' style='text-align: right;''>1</td>",
            "<td id='c_prix' style='text-align: right;'>"+ cadranDb["prix"] + "</td>",
            "</tr>",
            "<tr class='l3'>",
            "<td id='option'>Procédé : " + procede + "</td>",
            "<td id='o_nb' style='text-align: right;''>1</td>",
            "<td id='o_prix' style='text-align: right;'>50.00</td>",
            "</tr>",
            this.featuresToRow(features),
            "<tr>",
            "<td></td>",
            "<th>TOTAL</th>",
            "<td id='total' style='font-weight: bold; background-color: #ededed; text-align: right; border: thin solid black; '>"+ prix +" €</td>",
            "</tr>",
            "</table>",
            "<p style='position: absolute; bottom: 0; margin-bottom: 15mm;'>Conditions et modalités de paiement<br>Le paiement est dû dans 15 jours<br><br>Caisse d’Epargne<br>IBAN: FR12 1234 5678<br>SWIFT/BIC: ABCDFRP1XXX</p>",
            "</div>",
            "</body>",
            "</html>"
        );
        try {
            const browser: puppeteer.Browser = await puppeteer.launch({args: ['--no-sandbox','--disable-setuid-sandbox']
        ,headless: true});
        const page: puppeteer.Page = await browser.newPage();
        await page.setContent(templateHtml.join(""));
        const pdf: Buffer = await page.pdf({format: 'A4'});
        const snippet64: string = await page.screenshot({encoding: 'base64', fullPage: true});
        await browser.close();
        const pdfToBase64: string = await pdf.toString('base64');
        //console.log(pdfToBase64);
        return {pdf: pdfToBase64, snippet: snippet64};
        }
        catch(err) {
            console.log(err)
            console.log("puppeteer fait tout péter sur heroku")
        }
    }
}