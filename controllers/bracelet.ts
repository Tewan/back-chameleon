import { Controller, Route, Tags, OperationId, SuccessResponse, Response,Get, Query } from 'tsoa'

import { IResponse } from '../interfaces/IResponse'
import { BraceletModel } from '../models/bracelet';
import httpStatus from '../utils/HttpStatus';

@Route('/bracelet')
@Tags('bracelet')
export class BraceletController extends Controller {
    @Get('/')
    @OperationId('getBraceletParLibCourt')
    @SuccessResponse('200', 'Récupération du bracelet')
    @Response<IResponse>('500', 'Erreur dans la récupération du bracelet')
    public async getBraceletParLibCourt(@Query()libCourt: string): Promise<IResponse> {
        try {
            let bracelet = await BraceletModel.findOne({lib_court: libCourt});
            let res: IResponse = {message: httpStatus[200], statusCode: httpStatus.OK, data: bracelet}
            this.setStatus(httpStatus.OK);
            return res;
        } catch(err) {
            let resErr: IResponse = {message: httpStatus[500], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
                data: err};
            return resErr;
        }
    }
}