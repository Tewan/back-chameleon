import { Route, Get, Controller, Post, Patch, Delete, SuccessResponse, Tags, Response, OperationId, Request } from 'tsoa';
import * as bcrypt from 'bcryptjs'
import * as JWT from 'jsonwebtoken'
import * as fs from 'fs'

import { UserModel } from "../models/user"
import { IResponse } from '../interfaces/IResponse'
import httpStatus from '../utils/HttpStatus'
import { DevisModel } from '../models/devis';
import { FactureModel } from '../models/factures'

const saltRounds: number = 10;
const secret: string = 'secret';

@Route('/user')
@Tags('User')
export class UserController extends Controller {
    @Post('/login')
    @OperationId('loginUser')
    @SuccessResponse('200', 'Connexion réussie')
    @Response<IResponse>('404', 'Aucun utilisateur trouvé')
    @Response<IResponse>('403', 'Mauvais mot de passe')
    @Response<IResponse>('500', 'Erreur incroyable, je saurais pas dire honnêtement')
    public async login(@Request() req): Promise<IResponse> {
        try {
            let user = await UserModel.findOne({email: req.fields.email});
            if (!user) {
                let responseNoUserFound: IResponse = {message: httpStatus[404], statusCode: httpStatus.NOT_FOUND,
                    data: ''};
                this.setStatus(httpStatus.NOT_FOUND);
                return responseNoUserFound;
            }
            else {
                let isMatch: boolean = await bcrypt.compare(req.fields.password, user['password']);
                if (!isMatch) {
                    let responseNoMatch: IResponse = {message: httpStatus[403], statusCode: httpStatus.FORBIDDEN,
                        data: isMatch};
                    this.setStatus(httpStatus.FORBIDDEN);
                    return responseNoMatch;
                }
                else {
                    let token: object = {success: 'JWT', token: JWT.sign({id: user['_id']}, secret)};
                    let responseOk: IResponse = {message: httpStatus[200], statusCode: httpStatus.OK,
                        data: token};
                    this.setStatus(httpStatus.OK);
                    return responseOk;
                }
            }
        }
        catch(err) {
            let responseErr: IResponse = {message: httpStatus[500], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
                data: err};
            this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
            return responseErr;
        }
    }

    @Post('/')
    @OperationId('createUser')
    @SuccessResponse('200', 'Utilisateur créé')
    @Response<IResponse>('500', 'Erreur MongoDB lors de la création')
    public async createUser(@Request() req): Promise<IResponse> {
        try {
            let img = fs.readFileSync(req.files.avatar.path);
            let encoded_img = img.toString('base64');
            let cryptedPassword: string = bcrypt.hashSync(req.fields.password, saltRounds);
            let newUser = new UserModel({email: req.fields.email, password: cryptedPassword,
                adress: req.fields.adress, phoneNumber: req.fields.phoneNumber, companyName: req.fields.companyName,
                avatar: encoded_img});
            await newUser.save();
            let responseOk: IResponse = {message: httpStatus[201], statusCode: httpStatus.CREATED,
                data: newUser};
            this.setStatus(httpStatus.CREATED);
            return responseOk;
        }
        catch(err) {
            let responseErr: IResponse = {message: httpStatus[500], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
                data: err};
            this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
            return responseErr;
        }
    }

    @Get('/')
    @OperationId('getUsers')
    @SuccessResponse('200', 'Récupération des utilisateurs')
    @Response<IResponse>('500', 'Erreur dans la récupération des utilisateurs')
    public async getUsers(): Promise<IResponse> {
        try {
            let users:any = await UserModel.find();
            let responseOk: IResponse = {message: httpStatus['200'], statusCode: httpStatus.OK, data: users};
            this.setStatus(httpStatus.OK);
            return responseOk;
        }
        catch(err) {
            let responseErr: IResponse = {message: httpStatus['500'], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
            data: err};
            this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
            return responseErr;
        }
    }

    @Get('/{id}')
    @OperationId('getUser')
    @SuccessResponse('200', "Récupération d'un utilisateur")
    @Response<IResponse>('500', "Erreur dans la récupération d'un utilisateur")
    public async getUser(id:string): Promise<IResponse> {
        try {
            let user:any = await UserModel.findOne({_id: id});
            let responseOk: IResponse = {message: httpStatus['200'], statusCode: httpStatus.OK, data: user};
            this.setStatus(httpStatus.OK);
            return responseOk;
        }
        catch(err) {
            this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
            console.log(err);
        }
    }

    @Delete('/{id}')
    @SuccessResponse('200', "Suppression d'un utilisateur")
    @Response<IResponse>('500', "Erreur dans la suppression d'un utilisateur")
    public async deleteUser(id:string): Promise<IResponse> {
        try {
            let deletedUser: any = await UserModel.deleteOne({_id: id});
            let responseOk: IResponse = {message: httpStatus['200'], statusCode: httpStatus.OK, data: deletedUser};
            this.setStatus(httpStatus.OK);
            return responseOk;
        }
        catch(err) {
            let responseErr: IResponse = {message: httpStatus['500'], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
            data: err};
            this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
            return responseErr;
        }
    }

    @Patch('/{id}')
    @SuccessResponse('200', "Modification réussie")
    @Response<IResponse>('500', "Erreur dans la modification de l'objet")
    public async updateUser(id: string, @Request() req): Promise<IResponse> {
        try {
            let updatedUser = req.fields;
            if (req.fields.password) {
                let cryptedPassword: string = bcrypt.hashSync(req.fields.password, saltRounds);
                req.fields.password = cryptedPassword;
            }
            await UserModel.findOneAndUpdate({_id: id}, {$set: updatedUser});
            let responseOk: IResponse = {message: httpStatus['200'], statusCode: httpStatus.OK, data: updatedUser};
            this.setStatus(httpStatus.OK);
            return responseOk;
        }
        catch(err) {
            let responseErr: IResponse = {message: httpStatus['500'], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
            data: err};
            this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
            return responseErr;
        }
    }

    @Get('/{id}/devis')
    @SuccessResponse('200', "Récupération des devis")
    @Response<IResponse>('500', "Erreur dans la récupération des devis")
    public async getDevisByUser(id: string): Promise<IResponse> {
        try {
            let devis = await DevisModel.find({company_id: id});
            let responseOk: IResponse = {message: httpStatus['200'], statusCode: httpStatus.OK, data: devis};
            this.setStatus(httpStatus.OK);
            return responseOk;
        } catch (err) {
            let responseErr: IResponse = {message: httpStatus['500'], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
            data: err};
            this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
            return responseErr;
        }
    }

    @Get('/{id}/facture')
    @SuccessResponse('200', "Récupération des devis")
    @Response<IResponse>('500', "Erreur dans la récupération des devis")
    public async getFactureByUser(id: string): Promise<IResponse> {
        try {
            let facture = await FactureModel.find({company_id: id});
            let responseOk: IResponse = {message: httpStatus['200'], statusCode: httpStatus.OK, data: facture};
            this.setStatus(httpStatus.OK);
            return responseOk;
        } catch (err) {
            let responseErr: IResponse = {message: httpStatus['500'], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
            data: err};
            this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
            return responseErr;
        }
    }

}

