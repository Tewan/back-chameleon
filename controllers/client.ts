import { Controller, Route, Tags, Post, OperationId, SuccessResponse, Response, Request, Get, Query } from 'tsoa'

import { ClientModel } from '../models/client'
import { IResponse } from '../interfaces/IResponse'
import httpStatus from '../utils/HttpStatus'

@Route('/client')
@Tags('client')
export class ClientController extends Controller {

    // Ajout d'une commande à un client
    @Post('/')
    @OperationId('createClient')
    @SuccessResponse('201', 'Utilisateur créé')
    @Response<IResponse>('500', 'Erreur MongoDB lors de la création')
    public async createClient(@Request() req): Promise<IResponse> {
        let newClient = new ClientModel({lastname: req.fields.lastname, firstname: req.fields.firstname,
            email: req.fields.email, adress: req.fields.adress, phoneNumber: req.fields.phoneNumber,
            companies: req.fields.companies});
        try {
            await newClient.save();
            let responseOk: IResponse = {message: httpStatus[201], statusCode: httpStatus.CREATED,
                data: newClient};
            this.setStatus(httpStatus.CREATED);
            return responseOk;
        }
        catch(e) {
            let responseErr: IResponse = {message: httpStatus[500], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
                data: e};
            this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
            return responseErr;
        }
    }

    /*
    @Post('/{idClient}/order/{idOrder}')
    public async addOrderToClient(): Promise<IResponse> {

    }
    */
    @Get('/')
    @OperationId('getClientsByCompanyId')
    @SuccessResponse('200', 'Récupération des clients par id entreprise')
    @Response<IResponse>('500', 'Erreur MongoDB')
    public async getClientsByCompanyId(@Query() idCompany: string): Promise<IResponse> {
        try {
            let clients = await ClientModel.find({companies: idCompany});
            console.log(clients);
            let responseOk: IResponse = {message: httpStatus[200], statusCode: httpStatus.OK,
                    data: clients};
            this.setStatus(httpStatus.OK);
            return responseOk;
        } catch (e) {
            let responseErr: IResponse = {message: httpStatus[500], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
                data: e};
            this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
            return responseErr;
        }
    }

    @Get('/{id}')
    @OperationId('getUser')
    @SuccessResponse('200', "Récupération d'un client")
    @Response<IResponse>('500', "Erreur dans la récupération d'un client")
    public async getClient(id:string): Promise<IResponse> {
        try {
            let user:any = await ClientModel.findOne({_id: id});
            let responseOk: IResponse = {message: httpStatus['200'], statusCode: httpStatus.OK, data: user};
            this.setStatus(httpStatus.OK);
            return responseOk;
        }
        catch(err) {
            this.setStatus(httpStatus.INTERNAL_SERVER_ERROR);
            console.log(err);
        }
    }
}