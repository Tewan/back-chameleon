import { Controller, Route, Tags, Post, OperationId, SuccessResponse, Response, Request, Get, Query, Delete, Patch } from 'tsoa'

import { FactureModel } from '../models/factures'
import { IResponse } from '../interfaces/IResponse'

@Route('/facture')
@Tags('Facture')
export class FactureController extends Controller {

    @Get('/')
    public async getFactures(): Promise<IResponse> {
        let devis = await FactureModel.find();
        let res: IResponse = {message: '', statusCode: 200, data: devis}
        this.setStatus(200);
        return res;
    }

    @Delete('/{id}')
    public async deleteFacture(id: string): Promise<IResponse> {
        let deleteDevis = await FactureModel.deleteOne({_id: id});
        let res: IResponse = {message: '', statusCode: 200, data: deleteDevis}
        return res;
    }

    @Delete('/')
    public async deleteAllFactures(): Promise<IResponse> {
        let deletedDevis = await FactureModel.remove({});
        let res: IResponse = {message: 'Suppression de toutes les factures réussie', statusCode: 200, data: deletedDevis}
        this.setStatus(200)
        return res;
    }
}