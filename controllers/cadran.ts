import { Controller, Route, Tags, OperationId, SuccessResponse, Response,Get, Query } from 'tsoa'

import { IResponse } from '../interfaces/IResponse'
import { CadranModel } from '../models/cadran';
import httpStatus from '../utils/HttpStatus';

@Route('/cadran')
@Tags('cadran')
export class CadranController extends Controller {
    @Get('/')
    @OperationId('getCadransParCouleurEtForme')
    @SuccessResponse('200', 'Récupération du cadran')
    @Response<IResponse>('500', 'Erreur dans la récupération du cadran')
    public async getCadransParCouleurEtForme(@Query() couleur: string, @Query() forme: string): Promise<IResponse> {
        try {
            let cadran = await CadranModel.find({couleur: couleur.toLowerCase(), forme: forme.toLowerCase()});
            let res: IResponse = {message: httpStatus[200], statusCode: httpStatus.OK, data: cadran}
            this.setStatus(httpStatus.OK);
            return res;
        } catch(err) {
            let resErr: IResponse = {message: httpStatus[500], statusCode: httpStatus.INTERNAL_SERVER_ERROR,
                data: err};
            return resErr;
        }
    }
}