import { Controller, Route, Tags, Post, OperationId, SuccessResponse, Response, Request, Get, Query, Delete, Patch } from 'tsoa'
import * as puppeteer from 'puppeteer'
import * as moment from 'moment'

import { DevisModel } from '../models/devis'
import { FactureModel } from '../models/factures'
import { IResponse } from '../interfaces/IResponse'
import { ClientModel } from '../models/client';
import { OrderModel } from '../models/order';
import { UserModel } from '../models/user';
import { BraceletModel } from '../models/bracelet'
import { CadranModel } from '../models/cadran'
import httpStatus from '../utils/HttpStatus'

@Route('/devis')
@Tags('devis')
export class DevisController extends Controller {

    @Get('/')
    public async getDevis(): Promise<IResponse> {
        try {
            let devis = await DevisModel.find();
            let data: any = devis;
            let devisMap = await data.map(async (devi) => {
                try {
                    let client = await ClientModel.findById(devi['client_id']);
                    devi['client'] = client;
                }
                catch (errClient) {
                    console.log("Erreur récup client");
                }
            });
            let res: IResponse = {message: '', statusCode: 200, data: devisMap}
            this.setStatus(200);
            return res;
        } catch(err) {
            console.log("Erreur récupération des devis");
        }
    }

    @Patch('/{id}/signer')
    public async signerDevis(id: string): Promise<IResponse> {
        let update = {status: "SIGNÉ"}
        try {
            let devisUpdated = await DevisModel.findOneAndUpdate({_id: id}, {$set: update}, {new: true});
            console.log(devisUpdated);
            let res: IResponse = {message: "Signature réussie", statusCode: 200, data: devisUpdated}
            // Création de la facture
            let facture = await this.createFacture(id);
            // Récupération des infos de la commande
            let order = await this.getOrderInfo(facture['order_id']);
            // Récupération des infos de l'entreprise
            let company = await this.getCompanyInfo(facture['company_id']);
            // Récupération des infos du client
            let client = await this.getClientInfo(facture['client_id']);
            let clientFacture = {firstname: client['firstname'], lastname: client['lastname'], adress: client['adress']}
            // Génération du pdf et du screen du pdf
            let doc = await this.generatePdfAndScreen(clientFacture, facture['_id'], facture['createdAt'], 
                company['companyName'], company['adress'], order['montre']['bracelet'], order['montre']['cadran'],
                order['montre']['procede'], order['montre']['features'], order['prix']);
            // Mise à jour de la facture
            try {
                let factureUpdated = await FactureModel.findOneAndUpdate({_id: facture._id}, {$set: doc}, {new: true});
                this.setStatus(200);
                let resFactureUpdated: IResponse = {message: httpStatus[200], statusCode: httpStatus.OK, data: factureUpdated};
                return resFactureUpdated;
            } catch(err) {
                let resFactureUpdatedErr: IResponse = {message: "Génération du PDF échouée", statusCode: 500, data: err};
                return resFactureUpdatedErr;
            }
        }
        catch(err) {
            let resErr: IResponse = {message: "Signature échouée", statusCode: 500, data: err};
            return resErr;
        }
    }

    @Delete('/{id}')
    public async deleteDevis(id: string): Promise<IResponse> {
        let deleteDevis = await DevisModel.deleteOne({_id: id});
        let res: IResponse = {message: '', statusCode: 200, data: deleteDevis}
        return res;
    }

    @Delete('/')
    public async deleteAllDevis(): Promise<IResponse> {
        let deletedDevis = await DevisModel.remove({});
        let res: IResponse = {message: 'Suppression de tous les devis réussie', statusCode: 200, data: deletedDevis}
        this.setStatus(200)
        return res;
    }

    private async createFacture(id: string) {
        let devis = await DevisModel.findById(id);
        let newFacture = new FactureModel({order_id: devis['order_id'], client_id: devis['client_id'],
            company_id: devis['company_id'], devis_id: devis['_id']});
        try {
            let facture = await newFacture.save();
            return facture;
        }
        catch(err) {
            return err;
        }
    }
    private async generatePdfFacture() {
        // Récupération les infos de la commandes
        // génération
    }

    private async getOrderInfo(id) {
        try {
            let order = await OrderModel.findById(id);
            return order;
        }
        catch(err) {
            let resErr: IResponse = {message: "Impossible de récupérer les infos de la commande",
                statusCode: 500, data: err};
            return resErr;
        }
    }

    private async getCompanyInfo(id) {
        try {
            let company = await UserModel.findById(id);
            return company;
        }
        catch(err) {
            let resErr: IResponse = {message: "Impossible de récupérer les infos de l'entreprise",
                statusCode: 500, data: err};
            return resErr;
        }
    }

    private async getClientInfo(id) {
        try {
            let client = await ClientModel.findById(id);
            return client;
        }
        catch(err) {
            let resErr: IResponse = {message: "Impossible de récupérer les infos du client",
                statusCode: 500, data: err};
            return resErr;
        }
    }

    private featuresToRow(features: [string]): string {
        let templateHtml = []
        features.forEach(feature => {
            let tr: string = "<tr class='l4'/>";
            let td1: string = "<td id='option' style='width: 50%'>"+ feature +"</td>";
            let td2: string = "<td id='o_nb' style='text-align: right;''>1</td>";
            let td3: string = "<td id='o_prix' style='text-align: right;''>10.00</td>";
            let trFin: string = "</tr>";
            templateHtml.push(tr, td1, td2, td3, trFin);
        });
        let html: string = templateHtml.join("");
        return html;
    }
    
    private async generatePdfAndScreen(client, facture_id: string, date: string, companyName: string
        , companyAdress: string, bracelet: string, cadran, procede: string, features: [string], prix: number) {
    let braceletDb = await BraceletModel.findOne({lib_court: bracelet});
    let cadranDb = await CadranModel.findOne({forme: cadran.forme, couleur: cadran.couleur.toLowerCase()});
    let dateFacture = moment(date).format('DD/MM/YYYY');
    let templateHtml = [];
    templateHtml.push(
        "<!DOCTYPE html>",
        "<html>",
        "<head>",
        "<meta charset='UTF-8'>",
        "<meta name='viewport' content='width=device-width, initial-scale=1.0'>",
        "<title>Facture</title>",
        "</head>",
        "<body style='font-family: Arial, Helvetica, sans-serif; width: 210mm; height: 297mm; margin: auto;'>",
        "<div id='page' style='position: relative; width: 210mm; height: 297mm; box-sizing: border-box; padding-top: 15mm; padding-left: 20mm; padding-right: 20mm; padding-bottom: 15mm; background-color: white;'>",
        "<h1 style='float: right; margin: 0; font-size: 30px;'>FACTURE</h1>",
        "<h2 style='margin: 0; font-size: 30px;'>"+ client.firstname + " " + client.lastname + "</h2>",
        "<div style='clear: both;'></div>",
        "<table style='margin-top: 20px;'>",
        "<tr>",
        "<td id='adress_1'>" + client.adress + "</td>",
        "</tr>",
        "</table>",
        "<table style='float: right; text-align: right; margin-top: 150px;'>",
        "<tr>",
        "<th>Devis n°</th>",
        "<td id='devis_nb'>"+ facture_id + "</td>",
        "</tr>",
        "<th>Date du devis</th>",
        "<td id='devis_date'>" + dateFacture + "</td>",
        "</table>",
        "<p style='margin-top: 150px;'><strong>Facturé à</strong><br>" + companyName + "<br>" + companyAdress + "</p>",
        "<div style='clear: both;'></div>",
        "<table class='main' style ='margin-top: 50px; border-collapse: collapse; width: 100%;'>",
        "<style>",
        "html, body {height: 100%;}",
        ".l0, .l1, .l2, .l3, .l4, .l0 th, .l1 td, .l2 td, .l3 td, .l4 td{ border: thin solid black;}",
        ".main th, .main td { padding-top: 10px; padding-bottom: 10px;}",
        ".main td { padding-left: 15px; padding-right: 15px; padding-top: 10px; padding-bottom: 10px; }",
        "</style>",
        "<tr class='l0' style='background-color:#ededed;'>",
        "<th>DÉSIGNATION</th>",
        "<th>QUANTITÉ</th>",
        "<th>MONTANT</th>",
        "</tr>",
        "<tr class='l1'>",
        "<td id='bracelet'>" + braceletDb["lib_long"] + "</td>",
        "<td id='b_nb' style='text-align: right;''>1</td>",
        "<td id='b_prix' style='text-align: right;'>"+ braceletDb["prix"] +"</td>",
        "</tr>",
        "<tr class='l2'>",
        "<td id='cadran'>Cadran " + cadranDb["forme"] + "couleur " + cadranDb["couleur"].toLowerCase() +"</td>",
        "<td id='c_nb' style='text-align: right;''>1</td>",
        "<td id='c_prix' style='text-align: right;'>"+ cadranDb["prix"] + "</td>",
        "</tr>",
        "<tr class='l3'>",
        "<td id='option'>Procédé : " + procede + "</td>",
        "<td id='o_nb' style='text-align: right;''>1</td>",
        "<td id='o_prix' style='text-align: right;'>50.00</td>",
        "</tr>",
        this.featuresToRow(features),
        "<tr>",
        "<td></td>",
        "<th>TOTAL</th>",
        "<td id='total' style='font-weight: bold; background-color: #ededed; text-align: right; border: thin solid black; '>"+ prix +" €</td>",
        "</tr>",
        "</table>",
        "<p style='position: absolute; bottom: 0; margin-bottom: 15mm;'>Conditions et modalités de paiement<br>Le paiement est dû dans 15 jours<br><br>Caisse d’Epargne<br>IBAN: FR12 1234 5678<br>SWIFT/BIC: ABCDFRP1XXX</p>",
        "</div>",
        "</body>",
        "</html>"
    );
    try {
        const browser: puppeteer.Browser = await puppeteer.launch({args: ['--no-sandbox','--disable-setuid-sandbox']
    ,headless: true});
    const page: puppeteer.Page = await browser.newPage();
    await page.setContent(templateHtml.join(""));
    const pdf: Buffer = await page.pdf({format: 'A4'});
    const snippet64: string = await page.screenshot({encoding: 'base64', fullPage: true});
    await browser.close();
    const pdfToBase64: string = await pdf.toString('base64');
    //console.log(pdfToBase64);
    return {pdf: pdfToBase64, snippet: snippet64};
    }
    catch(err) {
        console.log(err)
        console.log("puppeteer fait tout péter sur heroku")
    }
    }
}