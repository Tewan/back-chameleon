export interface IResponse {
    message: string,
    statusCode: number,
    data: any
}