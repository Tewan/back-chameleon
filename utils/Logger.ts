import * as pino from 'pino'

export const logger = pino({
    name: 'chameleon',
    level: 'debug'
});